﻿DROP DATABASE IF EXISTS ejemplo2programacion;
CREATE DATABASE ejemplo2programacion;
USE ejemplo2programacion;

-- ej1a. Realizar un procedimiento almacenado que reciba un texto y un cararter.
-- Debe indicarte si ese caracrter esta en el texto. Utilizar LOCATE.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej1a(texto varchar(100), caracter char(1))
    BEGIN
      
      DECLARE vesta varchar(50) DEFAULT 'El caracter no aparece';

      IF (LOCATE(caracter, texto)<>0) THEN
        SET vesta = 'El caracter esta en el texto';
      END IF;

      SELECT vesta;

    END //
  DELIMITER ;

  CALL ej1a('racing','a');
  CALL ej1a('racing','e');


-- ej1b. Realizar un procedimiento almacenado que reciba un texto y un cararter.
-- Debe indicarte si ese caracrter esta en el texto. Utilizar POSITION.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej1b(texto varchar(100), caracter char(1))
    BEGIN
      
      DECLARE vesta varchar(50) DEFAULT 'El caracter no aparece';

      IF (POSITION(caracter IN texto)<>0) THEN
        SET vesta = 'El caracter esta en el texto';
      END IF;

      SELECT vesta;

    END //
  DELIMITER ;

  CALL ej1b('racing','a');
  CALL ej1b('racing','e');

-- ej2. Realiza un procedimiento almacenado que reciba un texto y un caracter.
-- te debe indicar el texto que haya antes de la primera vez que aparezca el caracter.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej2(texto varchar(100), caracter char(1))
    BEGIN
      
      DECLARE vresultado varchar(100) DEFAULT '';

      SET vresultado = SUBSTRING_INDEX(texto,caracter,1);

      SELECT vresultado;

    END //
  DELIMITER ;

  CALL ej2('racing santander','t');
  CALL ej2('telefono','a');

-- ej3. Realiza un procedimiento almacenado que reciba tres numeros y dos argumentos de 
-- salida donde devuelva el número más grande y el número más pequeño de los tres pasados.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej3(num1 int, num2 int, num3 int, OUT grande int, OUT pequeño int)
    BEGIN
      
      SET grande = GREATEST(num1,num2,num3);
      SET pequeño = LEAST(num1,num2,num3);
  
    END //
  DELIMITER ;

  CALL ej3(7,25,12,@mayor,@menor);
  SELECT @mayor,@menor;


-- CREACION DE TABLA DATOS PARA PROCEDIMIENTOS 4,5,6,7 y 8
  CREATE OR REPLACE TABLE datos(
    datos_id int(11) NOT NULL AUTO_INCREMENT,
    numero1 int(11) NOT NULL,
    numero2 int(11) NOT NULL,
    suma varchar(255) DEFAULT NULL,
    resta varchar(255) DEFAULT NULL,
    rango varchar(5) DEFAULT NULL,
    texto1 varchar(25) DEFAULT NULL,
    texto2 varchar(25) DEFAULT NULL,
    junto varchar(255) DEFAULT NULL,
    longitud varchar(255) DEFAULT NULL,
    tipo int(11) NOT NULL,
    numero int(11) NOT NULL,
    PRIMARY KEY (datos_id)
  );

  ALTER TABLE datos
    ADD INDEX numero2_index(numero2);
  ALTER TABLE datos
    ADD INDEX numero_index(numero);
  ALTER TABLE datos
    ADD INDEX tipo_index(tipo);

-- ej4. Realiza un procedimiento almacenado que muestre cuantos numeros 1 y numero2 
-- son mayores de 50.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej4()
    BEGIN
      DECLARE vnum1 int DEFAULT 0;
      DECLARE vnum2 int DEFAULT 0;

      SET vnum1 = (SELECT COUNT(*) FROM datos WHERE numero1>50);
      SET vnum2 = (SELECT COUNT(*) FROM datos WHERE numero2>50);

      SELECT vnum1, vnum2;
                        
    END //
  DELIMITER ;

  CALL ej4();

-- ej5. Realizar un procedimiento almacenado que calcule la suma y la resta de numero1 
-- y numero2  de la tabla datos.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej5()
    BEGIN

      UPDATE datos SET suma = numero1+numero2;
      UPDATE datos SET resta = numero1-numero2;

    END //
  DELIMITER ;

  CALL ej5();

-- ej6. Realizar un procedimietno almacenado que primero ponga todos los valores de suma y 
-- resta a NULL y despues calcule la suma solamente si el numero1 es mayor que el numero2
-- y calcule la resta de numero2-numero1 si el numero2 es mayor o numero1-numero2 si es
-- mayor el numero1

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej6()
    BEGIN
      
      -- Poner suma y resta a NULL
      UPDATE datos SET suma = NULL;
      UPDATE datos SET resta = NULL;

      -- Comprobaciones para la realizacion de la suma y actualizacion del campo suma si se cumple
      UPDATE datos SET suma = numero1 + numero2 
        WHERE numero1 > numero2;

      -- Comprobaciones para la realizacion de la resta y actualizacion del campor resta si se cumple
      UPDATE datos SET resta = numero2 - numero1  
        WHERE numero2 > numero1;
      UPDATE datos SET resta = numero1 - numero2  
        WHERE numero1 >= numero2;

    END //
  DELIMITER ;

  CALL ej6();

  SELECT * FROM datos d;

-- ej7. Realizar un procedimiento almacenado que coloque en el campo junto el texto1, texto2
  
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej7()
    BEGIN
      
      UPDATE datos SET junto = CONCAT(texto1,", ",texto2);

    END //
  DELIMITER ;

  CALL ej7();

  SELECT * FROM datos d;

-- ej8. Realizar un procedimiento almacenado que coloque en el campo junto el valor NULL
-- Después debe colocar en el campo junto el texto1 + texto2 si el rango es A y si es 
-- rango B debe colocar texto1 + texto2. Si el rango es distinto debe colocar texto1 nada más.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej8()
    BEGIN
        
      UPDATE datos SET junto = NULL;

      UPDATE datos SET junto = 
        IF (rango = 'A', CONCAT(texto1,'-',texto2), 
          IF (rango = 'B', CONCAT(texto1,'+',texto2),
            texto1
          )
        );

    END //
  DELIMITER ;

  CALL ej8();

  SELECT * FROM datos d;